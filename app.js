const discord    = require("discord.js");
const client     = new discord.Client();
client.commands  = new discord.Collection();
client.cooldowns = new discord.Collection();
client.config    = require("./config.json");
const read       = require("fs-readdir-recursive");

const command_files = read("./commands").filter(file => file.endsWith(".js"));

for (let i in command_files) {
    const command = require(`./commands/${command_files[i]}`);
    console.log(`Loaded command ${command.name}`);
    client.commands.set(command.name, command);
}

const event_files = read("./events").filter(file => file.endsWith(".js"));

for (let i in event_files) {
    const event = require(`./events/${event_files[i]}`);

    console.log(`Loaded event ${event.event}`);
    client.on(event.event, event.execute.bind(null, client));
}

client.login(client.config.token);
