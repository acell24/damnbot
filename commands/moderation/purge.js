module.exports = {
    name: "purge",
    aliases: ["wipe", "clean"],
    category: "Moderation",
    usage: "<prefix>purge <number of messages>",
    args: true,
    run(client, message, args) {
        if(message.guild === null)return;

        if(!message.member.hasPermission('MANAGE_MESSAGES'))
        return message.reply("Sorry, but you don't have the permission to delete messages.");

        let messagecount = parseInt(args[0]);

        while (messagecount > 0) {
            if (messagecount - 100 <= 0) { var count = messagecount; }
            else { var count = 100; }

            messagecount -= count;

            message.channel.messages.fetch({
                limit: count
            }).then(messages => message.channel.bulkDelete(messages));
        }
    }
};
