module.exports = {
  event: "ready",
  execute(client) {
    console.log(`I'm online!\nServing ${client.users.cache.size} users in ${client.guilds.cache.size} servers.`);
  }
};
